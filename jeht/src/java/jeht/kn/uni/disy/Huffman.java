/*
Copyright (c) 2009 thomas.zink _at_ uni-konstanz _dot_ de

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package jeht.kn.uni.disy;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.TreeSet;

/**
 * @author zink
 *
 */
public final class Huffman<K> {
	private Node<K> root;
	private Hashtable<K,String> symbols;
	private Hashtable<String,K> codes;
	
	public Huffman(final Node<K> root) {
		this.root = root;
	}
	
	public Huffman(final Hashtable<K,Integer> distribution) {
		this.root = buildTree(distribution);
		this.symbols = new Hashtable<K,String>();
		this.codes = new Hashtable<String,K>();
		buildSymboltable("", root);
		buildCodetable();
	}
	
	private final Node<K> buildTree(final Hashtable<K,Integer> distribution) {
		final TreeSet<Node<K>> forest = new TreeSet<Node<K>>();
		for (K key : distribution.keySet()) {
			forest.add(new Node<K>(key,distribution.get(key)));
		}
		while (forest.size()>1) {
			Node<K> tree0 = forest.first();
			forest.remove(tree0);
			Node<K> tree1 = forest.first();
			forest.remove(tree1);
			Node<K> root = new Node<K>(tree0,tree1);
			forest.add(root);
		}
		return forest.first();
	}
	
	private final void buildSymboltable(String code, Node<K> node) {
		if (node.getSymbol()!=null) {
			if (code.equals("")) {
				code += "0";
			}
			this.symbols.put(node.getSymbol(), code);
		}
		else {
			if (node.getLeft()!=null) {
				buildSymboltable(code + "0", node.getLeft());
			}
			if (node.getRight()!=null) {
				buildSymboltable(code + "1", node.getRight());
			}
		}
	}
	
	private final void buildCodetable() {
		for (K symbol : this.symbols.keySet()) {
			this.codes.put(this.symbols.get(symbol), symbol);
		}
	}
	
	@SuppressWarnings("unchecked")
	public final K[] decode(final String crypt) {
		ArrayList<K> plain = new ArrayList<K>();
		String code = "";
		for (final Character c : crypt.toCharArray()) {
			code += c;
			K symbol = this.codes.get(code);
			if (symbol!=null) {
				plain.add(symbol);
				code = "";
			}
		}
		return (K[]) plain.toArray();
	}
	
	public final String encode(final K[] plain) {
		String crypt = "";
		for (K symbol : plain) {
			crypt += this.symbols.get(symbol);
		}
		return crypt;
	}
	
	@Override
	public String toString() {
		return root.toString();
	}

	public Node<K> getRoot() {
		return root;
	}

	public void setRoot(final Node<K> root) {
		this.root = root;
	}
	
	public Hashtable<K, String> getSymbols() {
		return symbols;
	}

	public void setSymbols(final Hashtable<K, String> symbols) {
		this.symbols = symbols;
	}

	public Hashtable<String, K> getCodes() {
		return codes;
	}

	public void setCodes(final Hashtable<String, K> codes) {
		this.codes = codes;
	}
	
	public String getCode(final K symbol) {
		return this.symbols.get(symbol);
	}
	
	public K getSymbol(final String code) {
		return this.codes.get(code);
	}

	public static void main(final String[] args) {
		Hashtable<Character,Integer> dist = new Hashtable<Character,Integer>();
		dist.put('A', 23);
		dist.put('C', 17);
		dist.put('E', 19);
		dist.put('S', 3);
		System.out.println(dist);
		Huffman<Character> huff = new Huffman<Character>(dist);
		System.out.println(huff);
		System.out.println(huff.getSymbols());
		System.out.println(huff.getCodes());
	}

}
