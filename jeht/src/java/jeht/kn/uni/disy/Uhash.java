/*
Copyright (c) 2009 thomas.zink _at_ uni-konstanz _dot_ de

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package jeht.kn.uni.disy;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.util.Random;
import java.util.Vector;

/**
 * 
 */

/**
 * @author zink
 *
 */
public final class Uhash<K> extends AbstractHash<K> {
	private final static long SEED = System.nanoTime();
	private final int bits;
	private final Integer[] lut;
	
	public Uhash(final int num, final int bits, final int range) {
		super(num);
		Random rnd = new Random(SEED);
		this.bits = bits;
		int len = num * bits;
		this.lut = new Integer[len];
		for (int i=0; i<len; i++) lut[i] = rnd.nextInt(range);
	}
	
	/* (non-Javadoc)
	 * @see AbstractHash#hash(java.lang.Object)
	 */
	@Override
	public final Vector<Integer> hash(final K key) {
		long k = ktol(key);
		final Vector<Integer> hashs = new Vector<Integer>(this.getNum());
		for (int n=0; n<this.getNum(); n++) hashs.add(hashfn(k,n));
		return hashs;
	}
	
	private final Integer hashfn(final long key, int offset) {
		Integer h = 0;
		for (int b=0; b<bits; b++, offset++)
			h ^= (((key>>b) & 1) == 1) ? lut[offset] : 0;
		return h;
	}
	
	private final long ktol(final K key) {
		long k;
		if (key instanceof Long) k = (Long)key;
		else if (key instanceof Number) k = ((Number)key).longValue();
		else if (key instanceof String) k =  new BigInteger(((String)key).getBytes()).longValue();
		else {
			ByteArrayOutputStream byteout = new ByteArrayOutputStream();
			try {
				ObjectOutputStream objectout = new ObjectOutputStream(byteout);
				objectout.writeObject( key ); 
			}
			catch(IOException e) { k = 0L; }
			k = new BigInteger(byteout.toByteArray()).longValue();
		}
		return k;
	}
	
	public final int getBits() { return bits; }
	
	public final Integer[] getLut() { return lut; }
}
