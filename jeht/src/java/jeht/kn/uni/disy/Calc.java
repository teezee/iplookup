/*
Copyright (c) 2009 thomas.zink _at_ uni-konstanz _dot_ de

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package jeht.kn.uni.disy;
/**
 * 
 */

/**
 * @author zink
 *
 */
public final class Calc {

	public static double log(final double x, final double base) {
		return Math.log(x)/Math.log(base);
	}
	
	public static int toBase(final double x, final double b) {
		return (int)Math.pow(b,(int)log(x,b));
	}
	
	public static int m_fht(final int n) {
		return (int) Math.pow(2,Math.ceil(log(12.8*n,2)));
	}
	
	public static int m_bf2(final int n, final int c) {
		return (int) Math.pow(2, Math.ceil(log(c*n,2)));
	}
	
	public static int m_bf(final int n, final int c) {
		return c*n;
	}
	
	public static int k_bf(final int n, final int m) {
		return (int) Math.ceil(m/(float)n * Math.log(2));
	}

	public static double fp_bf(final int n, final int m, final int k) {
		return Math.pow( Math.pow(1-(1-(1/(double)m)),(n*k)),k);
	}
	
	public static double fp_bf_e(final int n, final int m, final int k) {
		return Math.pow(Math.pow(1-Math.E, -(n*k)/(double)m),k);
	}

	public static int d_mht(final int n) {
		return (int) Math.ceil(log(log(n,2)+1,2));
	}
}
