/*
Copyright (c) 2009 thomas.zink _at_ uni-konstanz _dot_ de

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package jeht.kn.uni.disy;

/**
 * @author zink
 *
 */
public final class Node<K> implements Comparable<K> {
	private final Integer frequency;
	private final K symbol;
	private Node<K> left;
	private Node<K> right;

	public Node(K symbol, Integer frequency, Node<K> left, Node<K> right) {
		this.frequency = frequency;
		this.symbol = symbol;
		this.left = left;
		this.right = right;
	}
	
	public Node(K symbol, Integer frequency) {
		this.frequency = frequency;
		this.symbol = symbol;
		this.left = null;
		this.right = null;
	}
	
	public Node(Node<K> a, Node<K> b)
	{
		this.symbol = null;
		this.frequency = a.getFrequency() + b.getFrequency();
		if (a.compareTo(b)<0) {
			this.left = a;
			this.right = b;
		}
		else {
			this.left = b;
			this.right = a;
		}
	}

	public int compareTo(Object o) {
		@SuppressWarnings("unchecked")
		Node<K> other = (Node<K>) o;
		return this.frequency - other.getFrequency();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((frequency == null) ? 0 : frequency.hashCode());
		result = prime * result + ((symbol == null) ? 0 : symbol.hashCode());
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node<K> other = (Node<K>) obj;
		if (frequency == null) {
			if (other.frequency != null)
				return false;
		} else if (!frequency.equals(other.frequency))
			return false;
		if (symbol == null) {
			if (other.symbol != null)
				return false;
		} else if (!symbol.equals(other.symbol))
			return false;
		return true;
	}

	public Node<K> getLeft() {
		return left;
	}
	public void setLeft(Node<K> left) {
		this.left = left;
	}
	public Node<K> getRight() {
		return right;
	}
	public void setRight(Node<K> right) {
		this.right = right;
	}
	public Integer getFrequency() {
		return frequency;
	}
	public K getSymbol() {
		return symbol;
	}

	@Override
	public String toString() {
		return this.nodeString(0);
	}
	
	private String nodeString(Integer depth) {
		String tree = new String();
		for (int i=0; i<depth; i++) { 
			tree += "--";
		}
		tree += "(" + this.symbol + "," + this.frequency + ")\n";
		if (left!=null) {
			tree += left.nodeString(depth+1);
		}
		if (right!=null) {
			tree += right.nodeString(depth+1);
		}
		return tree;
	}
}
