/*
Copyright (c) 2009 thomas.zink _at_ uni-konstanz _dot_ de

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package jeht.kn.uni.disy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Random;

/**
 * 
 */

/**
 * @author zink
 *
 */
public final class CountingBloomFilter extends AbstractCountingBloomFilter<Short> {
	/**
	 * 
	 */
	public CountingBloomFilter(final int len) {
		super();
		this.filter = new Short[len];
		Arrays.fill(filter, (short)0);
	}

	@Override
	public void set(Short t, Integer... a) {
		for (final Integer i : a) { filter[i] = t; }
	}
	
	@Override
	public void clear() {
		for (int i=0; i<filter.length; i++) { filter[i] = 0; }
	}
	
	/* (non-Javadoc)
	 * @see AbstractCountingBloomFilter#inc(int[])
	 */
	@Override
	public final void inc(final Integer... a) {
		for (final Integer i: a) { filter[i]++; }
	}
	
	/* (non-Javadoc)
	 * @see AbstractCountingBloomFilter#dec(int[])
	 */
	@Override
	public final void dec(final Integer... a) {
		for (final Integer i : a) { if (filter[i]>=0) filter[i]--; }
	}

	
	/* (non-Javadoc)
	 * @see AbstractCountingBloomFilter#get(int[])
	 */
	@Override
	public final ArrayList<Short> get(final Integer... a) {
		final ArrayList<Short> r = new ArrayList<Short>();
		for (final Integer i : a) { r.add(filter[i]); }
		return r;
	}
	
	@Override
	public final Short getMax() {
		Short max = 0;
		for (final Short i : filter) { max = (short)Math.max(i, max); } 
		return max;
	}
	
	@Override
	public final int getBits() {
		return getCounterwidth()*filter.length;
	}

	@Override
	public final int getCounterwidth() {
		return (int)Math.ceil(Calc.log(getMax()+1,2));
	}

	@Override
	public Hashtable<Short, Integer> getDistribution() {
		Hashtable<Short, Integer> dist = new Hashtable<Short, Integer>();
		for (Short i : filter) {
			Integer freq = dist.get(i);
			if (freq==null) {
				dist.put(i, 1);
			}
			else dist.put(i, ++freq);
		}
		return dist;
	}

	/**
	 * @param args
	 */
	public final static void main(final String[] args) {
		Random rnd = new Random();
		int len = rnd.nextInt(90)+10;
		System.out.print("Test Counting Bloom Filter\n");
		System.out.print("==========================\n");
		CountingBloomFilter cbf = new CountingBloomFilter(len);
		System.out.print("Created empty filter.\n");
		for (int i: cbf.getFilter()) assert i == 0;
		System.out.println(cbf.toString());
		System.out.print("Increase counters\n");
		for (int i=0; i<cbf.length(); i++) cbf.inc(i);
		for (int i: cbf.getFilter()) assert i == 1;
		Integer[] a = {0,1,2,3,4,5,6,7,8,9};
		cbf.inc(a);
		//Integer[] r = cbf.get(a);
		ArrayList<Short> r = cbf.get(a);
		for (Short i: r) assert i==2;
		System.out.println(cbf.toString());
		System.out.print("Decrease counters\n");
		cbf.dec(a);
		r = cbf.get(a);
		for (Short i: r) assert i==1;
		for (int i=0; i<cbf.length(); i++) cbf.dec(i);
		for (int i: cbf.getFilter()) assert i == 2;
		System.out.println(cbf.toString());
	}
	
	/*public static void main() {
		CountingBloomFilter cbf = new CountingBloomFilter((int)Math.pow(2,24));
	}*/
}
